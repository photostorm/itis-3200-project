/**
 * Main: This class is used to choose how the program will run
 * 
 * @author Justin E. Ervin
 * @version 4/15/2015
 */

package net.sslrsa.project;

import java.awt.EventQueue;
import java.security.KeyPair;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JOptionPane;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Main {

	/**
	 * Execution of this program starts in the main( ) method
	 * 
	 * @param Java arguments
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Security.addProvider(new BouncyCastleProvider());
		String providerName = "BC";
		String programVersion = null;

		// Setting the location of the certificates
		CertificateFile.setProgramLocation(JOptionPane.showInputDialog(null, "Enter location of the certificates: (none for default)"));
		CertificateFile.setProgramLocation(CertificateFile.getProgramLocation() != null ? CertificateFile.getProgramLocation() : "");

		// Selecting which type of the program to run
		do {
			programVersion = JOptionPane.showInputDialog(null, "0 - Client | 1 - Server | 2 - CA Server");

			if (programVersion == null) {
				break;
			}
		} while (!programVersion.equalsIgnoreCase("0") && !programVersion.equalsIgnoreCase("1") && !programVersion.equalsIgnoreCase("2"));

		if (Security.getProvider(providerName) == null) {
			System.out.println(providerName + " provider not installed");
		} else if (programVersion != null) {
			if (programVersion.equalsIgnoreCase("0")) {
				// Running the program as the client
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							ClientGUI window = new ClientGUI();
							window.frmClientGUI.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			} else if (programVersion.equalsIgnoreCase("1")) {
				// Running the program as the server
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							ServerGUI window = new ServerGUI();
							window.frmServerGUI.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			} else if (programVersion.equalsIgnoreCase("2")) {
				// Generating all of the certificates and running the program as the Trusted CA
				// The date for the other certificates
				Calendar calNormal = Calendar.getInstance();
				calNormal.set(2014, 6, 7, 11, 59, 59);

				Date notBeforeNormal = calNormal.getTime();
				calNormal.add(Calendar.YEAR, 3);
				Date notAfterNormal = calNormal.getTime();

				// The date for the expired certificate
				Calendar calExpired = Calendar.getInstance();
				calExpired.set(2010, 6, 7, 11, 59, 59);

				Date notBeforeExpired = calExpired.getTime();
				calExpired.add(Calendar.YEAR, 2);
				Date notAfterExpired = calExpired.getTime();

				// Root Certificate
				KeyPair kp = CryptoUtil.generateRSAKeyPair(2048);
				X509Certificate cert = CertificateFile.generateRootCert("Thawte Server CA 2", "Certification Services Division", "Thawte Consulting 2 cc", "Cape Town", "Western Cape", "ZA", kp);

				// Correct Certificate
				KeyPair kp2 = CryptoUtil.generateRSAKeyPair(2048);
				X509Certificate cert2 = CertificateFile.generateChildCert(cert, kp, notBeforeNormal, notAfterNormal, "Bob's Market", "Bob's Market IT Division", "Bob's Market Inc", "Raleigh", "North Carolina", "US", kp2);

				// Mismatch public key in certificate
				KeyPair kp3 = CryptoUtil.generateRSAKeyPair(2048);
				X509Certificate cert3 = CertificateFile.generateChildCert(cert, kp, notBeforeNormal, notAfterNormal, "Bob's Market", "Bob's Market IT Division", "Bob's Market Inc", "Raleigh", "North Carolina", "US", kp2);

				// Expired Certificate
				KeyPair kp4 = CryptoUtil.generateRSAKeyPair(2048);
				X509Certificate cert4 = CertificateFile.generateChildCert(cert, kp, notBeforeExpired, notAfterExpired, "Bob's Market", "Bob's Market IT Division", "Bob's Market Inc", "Raleigh", "North Carolina", "US", kp4);

				// Not signed by the root certificate
				KeyPair kp5 = CryptoUtil.generateRSAKeyPair(2048);
				X509Certificate cert5 = CertificateFile.generateChildCert(cert, kp5, notBeforeNormal, notAfterNormal, "Bob's Market", "Bob's Market IT Division", "Bob's Market Inc", "Raleigh", "North Carolina", "US", kp5);

				// Saving the root certificate to the hard drive
				CertificateFile.saveKey(kp, "root");
				CertificateFile.saveCertificate(cert, "root");

				// Saving the normal certificate to the hard drive
				CertificateFile.saveKey(kp2, "normal");
				CertificateFile.saveCertificate(cert2, "normal");

				// Saving the mismatch certificate to the hard drive
				CertificateFile.saveKey(kp3, "mismatch");
				CertificateFile.saveCertificate(cert3, "mismatch");

				// Saving the expired certificate to the hard drive
				CertificateFile.saveKey(kp4, "expired");
				CertificateFile.saveCertificate(cert4, "expired");

				// Saving the certificate not signed by the root`s private key to the hard drive
				CertificateFile.saveKey(kp5, "notsigned");
				CertificateFile.saveCertificate(cert5, "notsigned");

				JOptionPane.showMessageDialog(null, "All certificates have been generated");
			}
		}
	}
}
