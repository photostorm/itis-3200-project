/**
 * Client: This class holds functions for the client
 * 
 * @author Justin E. Ervin
 * @version 4/15/2015
 */

package net.sslrsa.project;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.apache.commons.codec.binary.Base64;

public class Client {
	public class MessagesThread extends Thread {
		@Override
		public void run() {
			try {
				// Reading incoming responses
				while (isServerConnected()) {
					readResponse(br.readLine());
				}
			} catch (Exception ex) {
			}
		}

	}

	public static enum RequestType {
		CERTIFICATE, SESSION_KEY, MESSAGE, SESSION_END;
	}

	private X509Certificate[] certificates = new X509Certificate[2];
	private SecretKey sessionKey = null;
	private String serverAddress = "";
	private int serverPort = 26850;
	private boolean serverConnected = false;
	private PrintWriter pw = null;
	private BufferedReader br = null;
	private Socket client = null;
	private ClientGUI handleGUI = null;

	/**
	 * Constructor for objects of Client class
	 * 
	 * @param the reference to the ClientGUI class
	 */
	public Client(ClientGUI handleGUI) {
		this.handleGUI = handleGUI;
	}

	/**
	 * This method checks the period of validity
	 * 
	 * @param the object of the certificate
	 * @return true or false if the certificate is still valid
	 */
	public boolean checkDate(X509Certificate crt) {
		try {
			crt.checkValidity();
			return false;
		} catch (Exception e) {
			return true;
		}
	}

	/**
	 * This method checks whether or not the certificate was signed by the CA's private key
	 * 
	 * @param the object of the certificate
	 * @param the object of the CA's public key
	 * @return true or false if the certificate was signed by the CA's private key
	 */
	public boolean checkWithRoot(X509Certificate crt, PublicKey key) {
		try {
			crt.verify(key);
			return false;
		} catch (Exception e) {
			return true;
		}
	}

	/**
	 * This method will try to connect to the server
	 * 
	 * @param the name of the type of certificate which the user requested
	 */
	public void connectServer(String crtType) {
		if (serverAddress.length() > 0) {
			try {
				client = new Socket(serverAddress, serverPort);
				br = new BufferedReader(new InputStreamReader(client.getInputStream()));
				pw = new PrintWriter(client.getOutputStream(), true);

				this.setServerConnected(true);

				// send name to server
				getHandleGUI().printOutput("\nRequesting to send confidential information...");
				sendResponse("(" + RequestType.CERTIFICATE.name().toLowerCase().trim() + ") c{" + crtType + "}c");

				// create thread for listening for messages
				new MessagesThread().start();
			} catch (Exception ex) {
				disconnectServer(true);
				getHandleGUI().printOutput("Failed: Server timeout!" + ex.getMessage());
			}
		}
	}

	/**
	 * This method will try to disconnect from the server
	 * 
	 * @param Whether or not the client tell the server to end the session
	 */
	public void disconnectServer(boolean callback) {
		try {
			if (client != null) {
				if (callback) {
					if (!sendResponse("(" + RequestType.SESSION_END.name().toLowerCase().trim() + ")")) {
						getHandleGUI().printOutput("Failed: Failed to send disconnect");
					}
				}

				client.close();
				pw.close();
				br.close();
				client = null;
				pw = null;
				br = null;
			}

			setServerConnected(false);
		} catch (IOException e) {
			getHandleGUI().printOutput("Failed: Failed to send disconnect");
		}
	}

	/**
	 * This method will return the object of the certificate
	 * 
	 * @param the integer valve of the index
	 * @return the object of the certificate
	 */
	public X509Certificate getCertificate(int index) {
		return this.certificates.length > index && index >= 0 ? this.certificates[index] : null;
	}

	/**
	 * This method will return the reference to the ClientGUI class
	 * 
	 * @return the reference to the ClientGUI class
	 */
	public ClientGUI getHandleGUI() {
		return handleGUI;
	}

	/**
	 * This method will return the address of the server
	 * 
	 * @return the address of the server
	 */
	public String getServerAddress() {
		return serverAddress;
	}

	/**
	 * This method will return the current server port
	 * 
	 * @return the current server port
	 */
	public int getServerPort() {
		return serverPort;
	}

	/**
	 * This method will return the current session key
	 * 
	 * @return the current session key
	 */
	public SecretKey getSessionKey() {
		return sessionKey;
	}

	/**
	 * This method will return whether or not the client is connected to the server
	 * 
	 * @return Whether or not the client is connected to the server
	 */
	public boolean isServerConnected() {
		return serverConnected;
	}

	/**
	 * This method process all of the data from the client
	 * 
	 * @param the data from the client
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws NoSuchProviderException
	 * @throws CertificateException
	 */
	public void readResponse(String line) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, CertificateException {
		if (line != null) {
			if (line.toLowerCase().contains("(" + RequestType.CERTIFICATE.name().toLowerCase().trim() + ")")) {
				String serverCertificate = line.substring(line.indexOf("c{") + 2, line.indexOf("}c")).trim();
				getHandleGUI().printOutput("\nReceived the server's certificate");

				// Decode the certificate's content
				byte[] serverCertificateContent = Base64.decodeBase64(serverCertificate);
				CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
				X509Certificate serverCrt = (X509Certificate) certFactory.generateCertificate(new ByteArrayInputStream(serverCertificateContent));

				setCertificate(serverCrt, 1);

				// Verify the server's certificate
				getHandleGUI().printOutput("Verifying the server's certificate...");
				if (checkDate(serverCrt)) {
					getHandleGUI().printOutput("Failed: Server's certificate is expired");
					getHandleGUI().endSession(true, true);
				} else if (checkWithRoot(serverCrt, certificates[0].getPublicKey())) {
					getHandleGUI().printOutput("Failed: Server's certificate is invalid");
					getHandleGUI().endSession(true, true);
				} else {
					getHandleGUI().printOutput("Generating session key...");
					sessionKey = CryptoUtil.generateAESKey(128);

					getHandleGUI().printOutput("Generated session key: " + Base64.encodeBase64String(sessionKey.getEncoded()));

					getHandleGUI().printOutput("Encrypting session key with the server's certificate public key...");
					String encryptedKey = CryptoUtil.encryptRSA(serverCrt.getPublicKey(), Base64.encodeBase64String(sessionKey.getEncoded()));
					getHandleGUI().printOutput("Session key encrypted with the server's certificate public key: " + encryptedKey);

					// Send the session key to the server
					getHandleGUI().printOutput("Sending session key encrypted with server's certificate public key...");
					sendResponse("(" + RequestType.SESSION_KEY.name().toLowerCase().trim() + ") s{" + encryptedKey + "}s");
				}
			} else if (line.toLowerCase().contains("(" + RequestType.SESSION_KEY.name().toLowerCase().trim() + ")")) {
				String encryptedKey = line.substring(line.indexOf("s{") + 2, line.indexOf("}s")).trim();
				getHandleGUI().printOutput("\nReceived the session key back from the server");
				getHandleGUI().printOutput("The encrypted session key from the server: " + encryptedKey);

				getHandleGUI().printOutput("Decrypting the encrypted session key from the server...");
				String unencryptedKey;

				if (!encryptedKey.endsWith("=")) {
					unencryptedKey = encryptedKey;
				} else {
					// Decrypt the encrypted session key from client
					unencryptedKey = CryptoUtil.decryptAES(sessionKey, encryptedKey);
				}

				getHandleGUI().printOutput("The unencrypted session key from the server: " + unencryptedKey);

				// Verify the session key from the server
				getHandleGUI().printOutput("Verifying the session key from the server...");
				if (Base64.encodeBase64String(sessionKey.getEncoded()).equalsIgnoreCase(unencryptedKey)) {
					getHandleGUI().printOutput("The session key from the server is valid");
					getHandleGUI().sendMessageCommand();
				} else {
					getHandleGUI().printOutput("Failed: The session key from the server is invalid");
					getHandleGUI().endSession(true, true);
				}
			} else if (line.toLowerCase().contains("(" + RequestType.MESSAGE.name().toLowerCase().trim() + ")")) {
				// Process the message from the server
				String encryptedMessage = line.substring(line.indexOf("m{") + 2, line.indexOf("}m")).trim();
				getHandleGUI().printOutput("\nReceived a encrypted message back from the server");
				getHandleGUI().printOutput("Encrypted message from the server: " + encryptedMessage);

				// Decrypt the encrypted message from server
				getHandleGUI().printOutput("Decrypting the server's message...");
				String unencryptedMessage = CryptoUtil.decryptAES(sessionKey, encryptedMessage);
				getHandleGUI().printOutput("The server's message: " + unencryptedMessage);
			} else if (line.toLowerCase().contains("(" + RequestType.SESSION_END.name().toLowerCase().trim() + ")")) {
				// End the current session
				getHandleGUI().endSession(false, false);
			}
		}
	}

	/**
	 * This method sends a request to the server for the certificate
	 */
	public void sendRequestForCertificate() {
		System.out.println("Requesting to send confidential information...");
		sendResponse("(" + RequestType.CERTIFICATE.name().toLowerCase().trim() + ") c{normal}c");
	}

	/**
	 * This method sends data to the server
	 * 
	 * @param the data which will be sent to the server
	 */
	public boolean sendResponse(String data) {
		try {
			if (isServerConnected() && !(client == null)) {
				pw.println(data);
				return true;
			}
		} catch (Exception ex) {

		}

		return false;
	}

	/**
	 * This method will set the certificate with their corresponding index
	 * 
	 * @param the object of the certificate
	 * @param the integer valve of the index
	 */
	public void setCertificate(X509Certificate crt, int index) {
		if (this.certificates.length > index && index >= 0) {
			this.certificates[index] = crt;
		}
	}

	/**
	 * This method will set what is the current reference to the ClientGUI class
	 * 
	 * @param the reference to the ClientGUI class
	 */
	public void setHandleGUI(ClientGUI handleGUI) {
		this.handleGUI = handleGUI;
	}

	/**
	 * This method will set the address of the server
	 * 
	 * @param the address of the server
	 */
	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}

	/**
	 * This method will set whether or not the client is connected to the server
	 * 
	 * @param Whether or not the client is connected to the server
	 */
	public void setServerConnected(boolean serverConnected) {
		this.serverConnected = serverConnected;
	}

	/**
	 * This method will set the current server port
	 * 
	 * @param the current server port
	 */
	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	/**
	 * This method will set the current session key
	 * 
	 * @param the current session key
	 */
	public void setSessionKey(SecretKey sessionKey) {
		this.sessionKey = sessionKey;
	}
}
