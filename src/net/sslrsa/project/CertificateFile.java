/**
 * CertificateFile: This class holds functions for the certificates
 * 
 * @author Justin E. Ervin
 * @version 4/15/2015
 */

package net.sslrsa.project;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

public class CertificateFile {
	private final static Charset ENCODING = StandardCharsets.UTF_8;
	private static String programLocation = "";

	/**
	 * This method will generate the certificate from the root certificate
	 * 
	 * @param the object of the root certificate
	 * @param the object of the root public and private key pair
	 * @param the date of start of validity
	 * @param the date of expiration
	 * @param the common name for the entity
	 * @param the name of the unit of the organization
	 * @param the name of the organization
	 * @param the name of the local
	 * @param the name of the state
	 * @param the name of the country
	 * @param the object of the public and private key pair
	 * @return the object of the certificate
	 * 
	 * @throws Exception
	 */
	public static X509Certificate generateChildCert(X509Certificate cert, KeyPair keyRoot, Date notBefore, Date notAfter, String commonName, String organizationUnit, String organizationName, String local, String state, String country, KeyPair keyPair) throws Exception {
		X500Name subjName = new X500Name("C=" + country + ", ST=" + state + ", L=" + local + ", O=" + organizationName + ", OU=" + organizationUnit + ", CN=" + commonName);
		X500Name issuerName = new X500Name(cert.getIssuerX500Principal().getName());
		BigInteger serialNumber = BigInteger.valueOf(System.currentTimeMillis());

		X509v3CertificateBuilder certBldr = new JcaX509v3CertificateBuilder(issuerName, serialNumber, notBefore, notAfter, subjName, keyPair.getPublic());
		ContentSigner signer = new JcaContentSignerBuilder("SHA1withRSA").setProvider("BC").build(keyRoot.getPrivate());
		JcaX509CertificateConverter x509Cert = new JcaX509CertificateConverter();
		return x509Cert.setProvider("BC").getCertificate(certBldr.build(signer));
	}

	/**
	 * This method will generate the root certificate
	 * 
	 * @param the common name for the entity
	 * @param the name of the unit of the organization
	 * @param the name of the organization
	 * @param the name of the local
	 * @param the name of the state
	 * @param the name of the country
	 * @param the object of the public and private key pair
	 * @return the object of the certificate
	 * @throws OperatorCreationException
	 * @throws CertificateException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 */
	public static X509Certificate generateRootCert(String commonName, String organizationUnit, String organizationName, String local, String state, String country, KeyPair keyPair) throws OperatorCreationException, CertificateException, NoSuchAlgorithmException, NoSuchProviderException {
		X500Name subjName = new X500Name("C=" + country + ", ST=" + state + ", L=" + local + ", O=" + organizationName + ", OU=" + organizationUnit + ", CN=" + commonName);
		BigInteger serialNumber = BigInteger.valueOf(System.currentTimeMillis());
		Calendar cal = Calendar.getInstance();
		cal.set(2014, 6, 7, 11, 59, 59);

		Date notBefore = cal.getTime();
		cal.add(Calendar.YEAR, 10); // Expires in 10 years
		Date notAfter = cal.getTime();

		X509v3CertificateBuilder certBldr = new JcaX509v3CertificateBuilder(subjName, serialNumber, notBefore, notAfter, subjName, keyPair.getPublic());
		ContentSigner signer = new JcaContentSignerBuilder("SHA1withRSA").setProvider("BC").build(keyPair.getPrivate());
		JcaX509CertificateConverter x509Cert = new JcaX509CertificateConverter();
		return x509Cert.setProvider("BC").getCertificate(certBldr.build(signer));
	}

	/**
	 * This method will returns the encoding for the files
	 * 
	 * @return the encoding for the files
	 */
	public static Charset getEncoding() {
		return ENCODING;
	}

	/**
	 * This method will returns the location of all of the certificates
	 * 
	 * @return the location of all of the certificates
	 */
	public static String getProgramLocation() {
		return programLocation;
	}

	/**
	 * This method will read the certificate to the hard drive
	 * 
	 * @param the name of the certificate
	 * @return the object of the certificate
	 * @throws IOException
	 * @throws CertificateException
	 */
	public static X509Certificate readCertificate(String certificateName) throws IOException, CertificateException {
		// Header and Footer
		String cert_begin = "-----BEGIN CERTIFICATE-----";
		String end_cert = "-----END CERTIFICATE-----";
		StringBuilder certString = new StringBuilder();

		File certFile = new File(programLocation + certificateName + ".crt");

		if (certFile.exists()) {
			// Read the file contents
			try (Scanner scanner = new Scanner(certFile.toPath(), ENCODING.name())) {
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					if (line != null) {
						certString.append(line);
					}
				}
				scanner.close();
				certFile = null;
			} catch (IOException e) {
				e.printStackTrace();
			}

			// Converting the certificate from a Base64 string to a object
			byte[] content = Base64.decodeBase64(certString.toString().replace(cert_begin, "").replace(end_cert, "").trim());
			CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
			X509Certificate x509Cert = (X509Certificate) certFactory.generateCertificate(new ByteArrayInputStream(content));
			return x509Cert;
		} else {
			return null;
		}
	}

	/**
	 * This method will read the private key to the hard drive
	 * 
	 * @param the name of the certificate
	 * @return the object of the private key
	 * @throws InvalidKeySpecException
	 * @throws FileNotFoundException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws IOException
	 */
	public static PrivateKey readKey(String certificateName) throws InvalidKeySpecException, FileNotFoundException, NoSuchAlgorithmException, NoSuchProviderException, IOException {
		// Header and Footer
		String key_begin = "-----BEGIN RSA PRIVATE KEY-----";
		String end_key = "-----END RSA PRIVATE KEY-----";
		StringBuilder certString = new StringBuilder();

		File certFile = new File(programLocation + certificateName + ".pem");

		if (certFile.exists()) {
			// Read the file contents
			try (Scanner scanner = new Scanner(certFile.toPath(), ENCODING.name())) {
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					if (line != null) {
						certString.append(line);
					}
				}
				scanner.close();
				certFile = null;
			} catch (IOException e) {
				e.printStackTrace();
			}

			// Converting the key from a Base64 string to a object
			byte[] content = Base64.decodeBase64(certString.toString().replace(key_begin, "").replace(end_key, "").trim());
			return CryptoUtil.convertStringToPrivateKey(content);
		} else {
			return null;
		}
	}

	/**
	 * This method will save the certificate to the hard drive
	 * 
	 * @param the object of the certificate
	 * @param the name of the certificate
	 * @throws CertificateEncodingException
	 */
	public static void saveCertificate(X509Certificate crt, String certificateName) throws CertificateEncodingException {
		// Header and Footer
		String cert_begin = "-----BEGIN CERTIFICATE-----\n";
		String end_cert = "\n-----END CERTIFICATE-----";

		// Converting the certificate from a object to a Base64 string
		byte[] derCert = crt.getEncoded();
		String pemCertPre = new String(Base64.encodeBase64(derCert));
		String pemCert = cert_begin + pemCertPre + end_cert;

		File certFile = new File(programLocation + certificateName + ".crt");

		// Create a blank file for the certificate if there is no file
		if (!certFile.exists()) {
			try {
				certFile.createNewFile();
			} catch (Exception e) {

			}
		}

		PrintWriter outputFile = null;

		// Writing the certificate to the hard drive
		if (certFile.exists()) {
			try {
				outputFile = new PrintWriter(certFile);
				outputFile.println(pemCert);
				outputFile.close();
			} catch (FileNotFoundException e) {

			}
		}
	}

	/**
	 * This method will save the private key to the hard drive
	 * 
	 * @param the object of the public and private key pair
	 * @param the name of the certificate
	 */
	public static void saveKey(KeyPair keyPair, String certificateName) {
		// Header and Footer
		String key_begin = "-----BEGIN RSA PRIVATE KEY-----\n";
		String end_key = "\n-----END RSA PRIVATE KEY-----";

		// Converting the key from a object to a Base64 string
		byte[] derCert = keyPair.getPrivate().getEncoded();
		String pemCertPre = new String(Base64.encodeBase64(derCert));
		String pemCert = key_begin + pemCertPre + end_key;

		File certFile = new File(programLocation + certificateName + ".pem");

		// Create a blank file for the key if there is no file
		if (!certFile.exists()) {
			try {
				certFile.createNewFile();
			} catch (Exception e) {

			}
		}

		PrintWriter outputFile = null;

		// Writing the key to the hard drive
		if (certFile.exists()) {
			try {
				outputFile = new PrintWriter(certFile);
				outputFile.println(pemCert);
				outputFile.close();
			} catch (FileNotFoundException e) {

			}
		}
	}

	/**
	 * This method will change the location where the certificates are.
	 * 
	 * @param the location of the certificates
	 */
	public static void setProgramLocation(String programLocation) {
		CertificateFile.programLocation = programLocation;
	}
}
