/**
 * Server: This class holds functions for the server
 * 
 * @author Justin E. Ervin
 * @version 4/15/2015
 */

package net.sslrsa.project;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.apache.commons.codec.binary.Base64;

public class Server {
	private class HandleClient extends Thread {
		public X509Certificate currentCertificate;
		public PrivateKey certificateKey = null;
		public SecretKey sessionKey = null;
		public BufferedReader input = null;
		public PrintWriter output = null;

		/**
		 * Constructor for objects of HandleClient class
		 * 
		 * @param the reference to the Socket class
		 * @throws Exception
		 */
		public HandleClient(Socket client) throws Exception {
			// get input and output streams
			input = new BufferedReader(new InputStreamReader(client.getInputStream()));
			output = new PrintWriter(client.getOutputStream(), true);
			String line = null;

			line = input.readLine();

			if (line != null) {
				String certificateType = line.substring(line.indexOf("c{") + 2, line.indexOf("}c")).trim();
				getHandleGUI().printOutput("\nReceived a request to send confidential information");

				loadCertificate(certificateType);
				loadKey(certificateType);

				getHandleGUI().printOutput("Sending my certificate to the client");
				if (currentCertificate != null) {
					Thread.sleep(1000);
					sendResponse("(" + RequestType.CERTIFICATE.name().toLowerCase().trim() + ") c{" + new String(Base64.encodeBase64(currentCertificate.getEncoded())) + "}c");
				} else {
					getHandleGUI().printOutput("Failed: Failed to send certificate");
				}
			} else {
				getHandleGUI().printOutput("Failed: Failed to send certificate");
			}

			start();
		}

		/**
		 * This method loads the certificate
		 * 
		 * @param the name of the type of certificate
		 * @throws CertificateException
		 * @throws IOException
		 */
		private void loadCertificate(String crtType) throws CertificateException, IOException {
			if (crtType.equalsIgnoreCase("normal")) {
				// Loading the normal certificate
				getHandleGUI().printOutput("Loading normal certificate from the hard drive");
				X509Certificate normalCertfile = CertificateFile.readCertificate("normal");

				if (normalCertfile != null) {
					getHandleGUI().printOutput("Loaded normal certificate from the hard drive");
					currentCertificate = normalCertfile;
				} else {
					getHandleGUI().printOutput("Failed: Failed to load normal certificate from the hard drive");
					currentCertificate = null;
				}
			} else if (crtType.equalsIgnoreCase("mismatch")) {
				// Loading mismatch public key certificate
				getHandleGUI().printOutput("Loading mismatch public key certificate from the hard drive");
				X509Certificate mismatchCertfile = CertificateFile.readCertificate("mismatch");

				if (mismatchCertfile != null) {
					getHandleGUI().printOutput("Loaded mismatch public key certificate from the hard drive");
					currentCertificate = mismatchCertfile;
				} else {
					getHandleGUI().printOutput("Failed: Failed to load mismatch public key certificate from the hard drive");
					currentCertificate = null;
				}
			} else if (crtType.equalsIgnoreCase("expired")) {
				// Loading expired certificate
				getHandleGUI().printOutput("Loading expired certificate from the hard drive");
				X509Certificate expiredCertfile = CertificateFile.readCertificate("expired");

				if (expiredCertfile != null) {
					getHandleGUI().printOutput("Loaded expired certificate from the hard drive");
					currentCertificate = expiredCertfile;
				} else {
					getHandleGUI().printOutput("Failed: Failed to load expired certificate from the hard drive");
					currentCertificate = null;
				}
			} else if (crtType.equalsIgnoreCase("notsigned")) {
				// Loading the certificate that was not signed by the root certificate
				getHandleGUI().printOutput("Loading the certificate that was not signed by the root certificate from the hard drive");
				X509Certificate notsignedCertfile = CertificateFile.readCertificate("notsigned");

				if (notsignedCertfile != null) {
					getHandleGUI().printOutput("Loaded the certificate that was not signed by the root certificate from the hard drive");
					currentCertificate = notsignedCertfile;
				} else {
					getHandleGUI().printOutput("Failed: Failed to load the certificate that was not signed by the root certificate from the hard drive");
					currentCertificate = null;
				}
			}
		}

		/**
		 * This method loads the private key
		 * 
		 * @param the name of the type of certificate
		 * @throws InvalidKeySpecException
		 * @throws FileNotFoundException
		 * @throws NoSuchAlgorithmException
		 * @throws NoSuchProviderException
		 * @throws IOException
		 */
		private void loadKey(String crtType) throws InvalidKeySpecException, FileNotFoundException, NoSuchAlgorithmException, NoSuchProviderException, IOException {
			if (crtType.equalsIgnoreCase("normal")) {
				// Private key for the normal certificate
				getHandleGUI().printOutput("Loading private key for normal certificate from the hard drive");
				PrivateKey normalKeyfile = CertificateFile.readKey("normal");

				if (normalKeyfile != null) {
					getHandleGUI().printOutput("Loaded private key for the normal certificate from the hard drive");
					certificateKey = normalKeyfile;
				} else {
					getHandleGUI().printOutput("Failed: Failed to load private key for the normal certificate from the hard drive");
					certificateKey = null;
				}
			} else if (crtType.equalsIgnoreCase("mismatch")) {
				// Private key for the mismatch public key certificate
				getHandleGUI().printOutput("Loading private key for mismatch public key certificate from the hard drive");
				PrivateKey mismatchKeyfile = CertificateFile.readKey("mismatch");

				if (mismatchKeyfile != null) {
					getHandleGUI().printOutput("Loaded private key for the mismatch public key certificate from the hard drive");
					certificateKey = mismatchKeyfile;
				} else {
					getHandleGUI().printOutput("Failed: Failed to load private key for the mismatch public key certificate from the hard drive");
					certificateKey = null;
				}
			} else if (crtType.equalsIgnoreCase("expired")) {
				// Private key for the expired certificate
				getHandleGUI().printOutput("Loading private key for expired certificate from the hard drive");
				PrivateKey expiredKeyfile = CertificateFile.readKey("expired");

				if (expiredKeyfile != null) {
					getHandleGUI().printOutput("Loaded private key for the expired certificate from the hard drive");
					certificateKey = expiredKeyfile;
				} else {
					getHandleGUI().printOutput("Failed: Failed to load private key for the expired certificate from the hard drive");
					certificateKey = null;
				}
			} else if (crtType.equalsIgnoreCase("notsigned")) {
				// Private key for the certificate that was not signed by the root certificate
				getHandleGUI().printOutput("Loading private key for the certificate that was not signed by the root certificate from the hard drive");
				PrivateKey notsignedKeyfile = CertificateFile.readKey("notsigned");

				if (notsignedKeyfile != null) {
					getHandleGUI().printOutput("Loaded private key for the certificate that was not signed by the root certificate from the hard drive");
					certificateKey = notsignedKeyfile;
				} else {
					getHandleGUI().printOutput("Failed: Failed to load private key for the certificate that was not signed by the root certificate from the hard drive");
					certificateKey = null;
				}
			}
		}

		@Override
		public void run() {
			String line = null;

			// Reading incoming responses
			while (true) {
				try {
					if (input != null) {
						line = input.readLine();

						if (line != null) {
							if (line.toLowerCase().contains("(" + RequestType.CERTIFICATE.name().toLowerCase().trim() + ")")) {
								String certificateType = line.substring(line.indexOf("c{") + 2, line.indexOf("}c")).trim();
								getHandleGUI().printOutput("\nReceived a request to send confidential information");

								// Load the current certificate and private key
								loadCertificate(certificateType);
								loadKey(certificateType);

								// Send the certificate back to the client
								getHandleGUI().printOutput("Sending my certificate to the client");
								if (currentCertificate != null) {
									Thread.sleep(1000);
									sendResponse("(" + RequestType.CERTIFICATE.name().toLowerCase().trim() + ") c{" + new String(Base64.encodeBase64(currentCertificate.getEncoded())) + "}c");
								} else {
									getHandleGUI().printOutput("Failed: Failed to send certificate");
								}
							} else if (line.toLowerCase().contains("(" + RequestType.SESSION_KEY.name().toLowerCase().trim() + ")")) {
								String encryptedKey = line.substring(line.indexOf("s{") + 2, line.indexOf("}s")).trim();
								getHandleGUI().printOutput("\nReceived a encrypted session key from client");
								getHandleGUI().printOutput("The session key encrypted with public key: " + encryptedKey);

								// Decrypt the encrypted session key from client
								getHandleGUI().printOutput("Decrypting the encrypted session key from client...");
								String unencryptedKey = CryptoUtil.decryptRSA(certificateKey, encryptedKey);

								getHandleGUI().printOutput("The unencrypted session key: " + unencryptedKey);

								getHandleGUI().printOutput("Encrypting session key with its self...");
								String encryptedSessionKey;

								if (!unencryptedKey.endsWith("=")) {
									encryptedSessionKey = CryptoUtil.byteArrayToHexString(unencryptedKey.getBytes());
								} else {
									// Converts the string to the object for the AES key
									sessionKey = CryptoUtil.convertStringToSecretKey(Base64.decodeBase64(unencryptedKey));

									encryptedSessionKey = CryptoUtil.encryptAES(sessionKey, unencryptedKey);
								}

								getHandleGUI().printOutput("The session key encrypted with its self: " + encryptedSessionKey);

								getHandleGUI().printOutput("Sending session key encrypted with its self...");
								Thread.sleep(1000);
								sendResponse("(" + RequestType.SESSION_KEY.name().toLowerCase().trim() + ") s{" + encryptedSessionKey + "}s");
							} else if (line.toLowerCase().contains("(" + RequestType.MESSAGE.name().toLowerCase().trim() + ")")) {
								// Process the message from the client
								String encryptedMessage = line.substring(line.indexOf("m{") + 2, line.indexOf("}m")).trim();
								getHandleGUI().printOutput("\nReceived a encrypted message from client");
								getHandleGUI().printOutput("The encrypted message: " + encryptedMessage);

								// Decrypt the encrypted message from client
								getHandleGUI().printOutput("Decrypting the encrypted message from client...");
								String unencryptedMessage = CryptoUtil.decryptAES(sessionKey, encryptedMessage);
								getHandleGUI().printOutput("The unencrypted message: " + unencryptedMessage);

								// Create a new response and send it back to the client encrypt with
								// the session key
								getHandleGUI().printOutput("My response to the client's message: " + "I got your message of \"" + unencryptedMessage + "\".");
								getHandleGUI().printOutput("Encrypting my response...");
								String encryptedNewMessage = CryptoUtil.encryptAES(sessionKey, "I got your message of \"" + unencryptedMessage + "\".");
								getHandleGUI().printOutput("My encrypted response: " + encryptedNewMessage);

								getHandleGUI().printOutput("Sending my response back to the client encrypted with the current session key...");
								Thread.sleep(1000);
								sendResponse("(" + RequestType.MESSAGE.name().toLowerCase().trim() + ") m{" + encryptedNewMessage + "}m");
							} else if (line.toLowerCase().contains("(" + RequestType.SESSION_END.name().toLowerCase().trim() + ")")) {
								// End the current session
								input = null;
								output = null;
								clients.remove(this);
								getHandleGUI().printOutput("Client ended session");
							}
						}
					}
				} catch (IOException | CertificateException | InvalidKeySpecException | NoSuchAlgorithmException | NoSuchProviderException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchPaddingException | InterruptedException e) {
					e.getMessage();
				}
			}
		}

		/**
		 * This method sends data back to the client
		 * 
		 * @param the data which will be sent back to the client
		 */
		public void sendResponse(String data) {
			output.println(data);
		}
	}

	public class MessagesThread extends Thread {
		@Override
		public void run() {
			// Listening for requests
			try {
				server = new ServerSocket(26850);
				getHandleGUI().printOutput("Listening for requests...");
			} catch (Exception e) {

			}

			// Waiting for new clients
			while (isServerRunning) {
				try {
					Socket client = server.accept();
					HandleClient c = new HandleClient(client);
					clients.add(c);
				} catch (Exception e) {
					setServerRunning(false);
					break;
				}
			}

			for (HandleClient c : clients) {
				c.sendResponse("(" + RequestType.SESSION_END.name().toLowerCase().trim() + ")");
			}

			// Close the server
			if (server != null) {
				try {
					server.close();
				} catch (IOException e) {

				}
			}
		}
	}

	public static enum RequestType {
		CERTIFICATE, SESSION_KEY, MESSAGE, SESSION_END;
	}

	private ArrayList<HandleClient> clients = new ArrayList<HandleClient>();
	private ServerGUI handleGUI = null;
	private ServerSocket server = null;
	private boolean isServerRunning = true;

	/**
	 * Constructor for objects of Server class
	 * 
	 * @param the reference to the ServerGUI class
	 */
	public Server(ServerGUI handleGUI) {
		this.handleGUI = handleGUI;
	}

	/**
	 * This method will return the reference to the ServerGUI class
	 * 
	 * @return the reference to the ServerGUI class
	 */
	public ServerGUI getHandleGUI() {
		return handleGUI;
	}

	/**
	 * This method will return whether or not the server is running
	 * 
	 * @return whether or not the server is running
	 */
	public boolean isServerRunning() {
		return isServerRunning;
	}

	/**
	 * This method will start up the thread for the server
	 */
	public void process() {
		new MessagesThread().start();
	}

	/**
	 * This method will set what is the current reference to the ServerGUI class
	 * 
	 * @param the reference to the ServerGUI class
	 */
	public void setHandleGUI(ServerGUI handleGUI) {
		this.handleGUI = handleGUI;
	}

	/**
	 * This method will set whether or not the server is running
	 * 
	 * @param whether or not the server is running
	 */
	public void setServerRunning(boolean isServerRunning) {
		this.isServerRunning = isServerRunning;
	}
}
