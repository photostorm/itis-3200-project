/**
 * ClientGUI: This class is used to create the GUI for the client
 * 
 * @author Justin E. Ervin
 * @version 4/15/2015
 */

package net.sslrsa.project;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import net.sslrsa.project.Client.RequestType;

import org.apache.commons.codec.binary.Base64;

public class ClientGUI implements ActionListener {
	/**
	 * This method converts a string to a integer value
	 * 
	 * @param a string which contains integer
	 * @param the number which will return if there is a error
	 * @return
	 */
	public static int getInt(String string, int fallBack) {
		int i;

		try {
			i = Integer.parseInt(string);
		} catch (Exception e) {
			i = fallBack;
		}

		return i;
	}

	public JFrame frmClientGUI;
	private JTextField txtCommandInput;
	private JButton btnRefreshSession;
	private JButton btnViewRootCert;
	private JButton btnViewServerCert;
	private JButton btnEndSession;
	private JButton btnSend;
	private JTextArea txtOutput;
	private JButton btnSendRequest;
	private Client client;
	private String commandText;
	private JTextField txtCommand;
	private JScrollPane scrollTextOutput;
	private JButton btnSendMessage;

	/**
	 * Constructor for objects of ClientGUI class
	 */
	public ClientGUI() {
		setClient(new Client(this));
		initialize();

		printOutput("Loading root certificate from the hard drive");
		X509Certificate certfile = null;

		try {
			certfile = CertificateFile.readCertificate("root");
		} catch (CertificateException e) {

		} catch (IOException e) {

		}

		if (certfile != null) {
			printOutput("Loaded root certificate from the hard drive");
			getClient().setCertificate(certfile, 0);
		} else {
			printOutput("Failed: Failed to load root certificate from the hard drive");
		}

		sendRequestCommand();
	}

	/**
	 * This method will all action from the button
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnSend) {
			// Send Button
			try {
				processCommand(this.txtCommandInput.getText());
			} catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e1) {

			}
		} else if (e.getSource() == btnRefreshSession) {
			// Refresh the current session
			if (getClient().getCertificate(1) != null && getClient().getCertificate(0) != null) {
				if (getClient().getSessionKey() != null) {
					this.setCommandText("send_request");
					printOutput("Generating session key...");
					try {
						getClient().setSessionKey(CryptoUtil.generateAESKey(128));
					} catch (NoSuchAlgorithmException e1) {

					} catch (NoSuchProviderException e1) {

					}

					printOutput("Generated session key: " + Base64.encodeBase64String(getClient().getSessionKey().getEncoded()));

					printOutput("Encrypting session key with the server's certificate public key...");
					String encryptedKey = null;
					try {
						encryptedKey = CryptoUtil.encryptRSA(getClient().getCertificate(1).getPublicKey(), Base64.encodeBase64String(getClient().getSessionKey().getEncoded()));
					} catch (InvalidKeyException e1) {

					} catch (IllegalBlockSizeException e1) {

					} catch (BadPaddingException e1) {

					} catch (NoSuchAlgorithmException e1) {

					} catch (NoSuchProviderException e1) {

					} catch (NoSuchPaddingException e1) {

					}

					printOutput("Session key encrypted with the server's certificate public key: " + encryptedKey);

					printOutput("Sending session key encrypted with server's certificate public key...");
					getClient().sendResponse("(" + RequestType.SESSION_KEY.name().toLowerCase().trim() + ") s{" + encryptedKey + "}s");
				} else {
					JOptionPane.showMessageDialog(this.frmClientGUI, "There is currently invalid certificate stored from the server");
				}
			} else {
				JOptionPane.showMessageDialog(this.frmClientGUI, "There is currently no certificate stored from the server");
			}
		} else if (e.getSource() == btnSendRequest) {
			// Send request to the server
			sendRequestCommand();
		} else if (e.getSource() == btnEndSession) {
			// End the current session
			if (getClient().isServerConnected()) {
				endSession(true, false);
			} else {
				JOptionPane.showMessageDialog(this.frmClientGUI, "Session has not started yet");
			}
		} else if (e.getSource() == btnViewRootCert) {
			// View the root certificate
			if (getClient().getCertificate(0) != null) {
				printOutput("\n--- Opening Root Certificate ---");
				printOutput(getClient().getCertificate(0).toString());
			} else {
				JOptionPane.showMessageDialog(this.frmClientGUI, "There is currently no root certificate stored");
			}
		} else if (e.getSource() == btnViewServerCert) {
			// View the server's certificate
			if (getClient().getCertificate(1) != null) {
				printOutput("\n--- Opening Server's Certificate ---");
				printOutput(getClient().getCertificate(1).toString());
			} else {
				JOptionPane.showMessageDialog(this.frmClientGUI, "There is currently no certificate stored from the server");
			}
		} else if (e.getSource() == btnSendMessage) {
			// Change the command textbox to send a message
			if (getClient().getSessionKey() != null) {
				sendMessageCommand();
			} else {
				JOptionPane.showMessageDialog(this.frmClientGUI, "There is no session key");
			}
		}
	}

	/**
	 * This method will end the current session
	 * 
	 * @param Whether or not the client tell the server to end the session
	 * @param Whether or not the client should save the current certificate
	 */
	public void endSession(boolean callback, boolean savedCertificate) {
		getClient().disconnectServer(callback);
		getClient().setSessionKey(null);

		if (!savedCertificate) {
			getClient().setCertificate(null, 1);
		}

		this.setCommandText("send_request");
		this.txtCommand.setText("Send Request");
		printOutput("Session closed");
	}

	/**
	 * This method returns the reference to the Client class
	 * 
	 * @return the reference to the Client class
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * This method returns the current command in the command textbox
	 * 
	 * @return the current command in the command textbox
	 */
	public String getCommandText() {
		return commandText;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmClientGUI = new JFrame();
		frmClientGUI.getContentPane().setBackground(new Color(241, 241, 241));
		frmClientGUI.setForeground(Color.BLACK);
		frmClientGUI.setBackground(Color.WHITE);
		frmClientGUI.setResizable(false);
		frmClientGUI.setTitle("SSL Client");
		frmClientGUI.setBounds(100, 100, 599, 500);
		frmClientGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmClientGUI.getContentPane().setLayout(null);

		btnSendRequest = new JButton("Send Certificate Request");
		btnSendRequest.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnSendRequest.setForeground(Color.BLACK);
		btnSendRequest.setBackground(Color.WHITE);
		btnSendRequest.setBounds(12, 13, 182, 25);
		btnSendRequest.addActionListener(this);
		frmClientGUI.getContentPane().add(btnSendRequest);

		btnRefreshSession = new JButton("Refresh Session Key");
		btnRefreshSession.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnRefreshSession.setForeground(Color.BLACK);
		btnRefreshSession.setBackground(Color.WHITE);
		btnRefreshSession.setBounds(206, 13, 182, 25);
		btnRefreshSession.addActionListener(this);
		frmClientGUI.getContentPane().add(btnRefreshSession);

		btnEndSession = new JButton("End Session");
		btnEndSession.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnEndSession.setForeground(Color.BLACK);
		btnEndSession.setBackground(Color.WHITE);
		btnEndSession.setBounds(206, 51, 182, 25);
		btnEndSession.addActionListener(this);
		frmClientGUI.getContentPane().add(btnEndSession);

		btnViewRootCert = new JButton("View Root Certificate");
		btnViewRootCert.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnViewRootCert.setForeground(Color.BLACK);
		btnViewRootCert.setBackground(Color.WHITE);
		btnViewRootCert.setBounds(400, 13, 182, 25);
		btnViewRootCert.addActionListener(this);
		frmClientGUI.getContentPane().add(btnViewRootCert);

		btnViewServerCert = new JButton("View Server Certificate");
		btnViewServerCert.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnViewServerCert.setForeground(Color.BLACK);
		btnViewServerCert.setBackground(Color.WHITE);
		btnViewServerCert.setBounds(400, 51, 182, 25);
		btnViewServerCert.addActionListener(this);
		frmClientGUI.getContentPane().add(btnViewServerCert);

		txtOutput = new JTextArea();
		txtOutput.setEditable(false);
		txtOutput.setLineWrap(true);
		txtOutput.setFont(new Font("Tahoma", Font.PLAIN, 13));
		scrollTextOutput = new JScrollPane(txtOutput, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollTextOutput.setBounds(12, 89, 570, 326);
		frmClientGUI.getContentPane().add(scrollTextOutput);

		txtCommandInput = new JTextField();
		txtCommandInput.setBounds(130, 429, 343, 24);
		frmClientGUI.getContentPane().add(txtCommandInput);
		txtCommandInput.setColumns(10);

		btnSend = new JButton("Send");
		btnSend.setForeground(Color.BLACK);
		btnSend.setBackground(Color.WHITE);
		btnSend.setBounds(485, 428, 97, 25);
		btnSend.addActionListener(this);
		frmClientGUI.getContentPane().add(btnSend);

		txtCommand = new JTextField();
		txtCommand.setEditable(false);
		txtCommand.setBounds(12, 428, 106, 25);
		frmClientGUI.getContentPane().add(txtCommand);
		txtCommand.setColumns(10);

		btnSendMessage = new JButton("Send Message");
		btnSendMessage.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnSendMessage.setForeground(Color.BLACK);
		btnSendMessage.setBackground(Color.WHITE);
		btnSendMessage.setBounds(12, 51, 182, 25);
		btnSendMessage.addActionListener(this);
		frmClientGUI.getContentPane().add(btnSendMessage);
	}

	/**
	 * This method will print any text to the output textbox
	 * 
	 * @param the input
	 */
	public void printOutput(String input) {
		StringBuilder text = new StringBuilder(txtOutput.getText());

		for (String line : input.split("\n")) {
			text.append(" " + line + "\n");
		}

		txtOutput.setText(text.toString());

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				scrollTextOutput.getVerticalScrollBar().setValue(scrollTextOutput.getVerticalScrollBar().getMaximum());
			}
		});
	}

	/**
	 * This method will process all commands
	 * 
	 * @param the text input
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws NoSuchPaddingException
	 */
	public void processCommand(String input) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException {
		if (getCommandText() != null) {
			if (getCommandText().equalsIgnoreCase("send_request")) {
				// Send a request for the server's certificate
				if (getClient().getCertificate(0) != null) {
					String crtType = null;
					switch (getInt(input, 0)) {
					case 1:
						crtType = "normal";
						break;
					case 2:
						crtType = "mismatch";
						break;
					case 3:
						crtType = "expired";
						break;
					case 4:
						crtType = "notsigned";
						break;
					default:
						crtType = null;
						break;
					}

					if (crtType != null) {
						if (getClient().isServerConnected()) {
							printOutput("\nRequesting to send confidential information...");
							getClient().sendResponse("(" + RequestType.CERTIFICATE.name().toLowerCase().trim() + ") c{" + crtType + "}c");
							getClient().setSessionKey(null);
						} else {
							// Prompting the user for the server's IP address
							do {
								getClient().setServerAddress(JOptionPane.showInputDialog(null, "Enter server IP Address:"));

								if (getClient().getServerAddress() == null) {
									break;
								}
							} while (getClient().getServerAddress().length() < 0);

							if (getClient().getServerAddress() != null) {
								getClient().connectServer(crtType);
							}
						}
					}
				} else {
					JOptionPane.showMessageDialog(this.frmClientGUI, "There is currently no root certificate loaded");
				}
			} else if (getCommandText().equalsIgnoreCase("send_message")) {
				// Sending a message to the server
				if (getClient().getSessionKey() != null) {
					printOutput("Your message: " + input);
					printOutput("Encrypting your message...");
					String encryptedMessage = CryptoUtil.encryptAES(getClient().getSessionKey(), input);
					printOutput("Your encrypted message: " + encryptedMessage);

					printOutput("Sending your message encrypted with the current session key to the server...");
					getClient().sendResponse("(" + RequestType.MESSAGE.name().toLowerCase().trim() + ") m{" + encryptedMessage + "}m");
				} else {
					JOptionPane.showMessageDialog(this.frmClientGUI, "Sorry, could not encrypt the message because there is no session key");
				}
			}
		}
	}

	/**
	 * This method will tell the user to enter a message
	 */
	public void sendMessageCommand() {
		if (getCommandText() != null) {
			if (!getCommandText().equalsIgnoreCase("send_message")) {
				printOutput("\nEnter your message:");
			}
		} else {
			printOutput("\nEnter your message:");
		}

		this.setCommandText("send_message");
		this.txtCommand.setText("Send Message");
	}

	/**
	 * This method will show all the different of types of certificates
	 */
	public void sendRequestCommand() {
		if (getCommandText() != null) {
			if (!getCommandText().equalsIgnoreCase("send_request")) {
				printOutput("\nSelect a situation:");
				printOutput("1 - Normal certificate");
				printOutput("2 - Wrong public key in the certificate");
				printOutput("3 - Expired certificate");
				printOutput("4 - Not signed by the root certificate");
			}
		} else {
			printOutput("\nSelect a situation:");
			printOutput("1 - Normal certificate");
			printOutput("2 - Wrong public key in the certificate");
			printOutput("3 - Expired certificate");
			printOutput("4 - Not signed by the root certificate");
		}

		this.setCommandText("send_request");
		this.txtCommand.setText("Send Request");
	}

	/**
	 * This method will set the reference to the client object being used
	 * 
	 * @param the reference to the client object being used
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 * This method will set the current command in the command textbox
	 * 
	 * @param the text of the command
	 */
	public void setCommandText(String commandText) {
		this.commandText = commandText;
	}
}
