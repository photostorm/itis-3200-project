/**
 * ServerGUI: This class is used to create the GUI for the server
 * 
 * @author Justin E. Ervin
 * @version 4/15/2015
 */

package net.sslrsa.project;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class ServerGUI implements WindowListener {
	public JFrame frmServerGUI;
	private JTextArea txtOutput;
	private JScrollPane scrollTextOutput;
	private Server server;

	/**
	 * Default Constructor for objects of ServerGUI class
	 */
	public ServerGUI() {
		setServer(new Server(this));
		initialize();

		server.process();
	}

	/**
	 * This method returns the reference to the Server class
	 * 
	 * @return the reference to the Server class
	 */
	public Server getServer() {
		return server;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmServerGUI = new JFrame();
		frmServerGUI.getContentPane().setBackground(new Color(241, 241, 241));
		frmServerGUI.setForeground(Color.BLACK);
		frmServerGUI.setBackground(Color.WHITE);
		frmServerGUI.setResizable(false);
		frmServerGUI.setTitle("SSL Server");
		frmServerGUI.setBounds(100, 100, 599, 500);
		frmServerGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmServerGUI.addWindowListener(this);
		frmServerGUI.getContentPane().setLayout(null);

		txtOutput = new JTextArea();
		txtOutput.setEditable(false);
		txtOutput.setLineWrap(true);
		txtOutput.setFont(new Font("Tahoma", Font.PLAIN, 13));
		scrollTextOutput = new JScrollPane(txtOutput, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollTextOutput.setBounds(12, 51, 570, 401);
		frmServerGUI.getContentPane().add(scrollTextOutput);
	}

	/**
	 * This method will print any text to the output textbox
	 * 
	 * @param the input
	 */
	public void printOutput(String input) {
		StringBuilder text = new StringBuilder(txtOutput.getText());

		for (String line : input.split("\n")) {
			text.append(" " + line + "\n");
		}

		txtOutput.setText(text.toString());

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				scrollTextOutput.getVerticalScrollBar().setValue(scrollTextOutput.getVerticalScrollBar().getMaximum());
			}
		});
	}

	/**
	 * This method will set the reference to the server object being used
	 * 
	 * @param the reference to the server object being used
	 */
	public void setServer(Server server) {
		this.server = server;
	}

	/**
	 * This method is listening for events related to the window is activated. This is also a
	 * required method of the WindowListener class.
	 */
	@Override
	public void windowActivated(WindowEvent e) {

	}

	/**
	 * This method is listening for events related to the window is closed. This is also a required
	 * method of the WindowListener class.
	 */
	@Override
	public void windowClosed(WindowEvent e) {

	}

	/**
	 * This method is listening for events related to the window is closing. This is also a required
	 * method of the WindowListener class.
	 */
	@Override
	public void windowClosing(WindowEvent e) {
		if (server != null) {
			server.setServerRunning(false);
		}
	}

	/**
	 * This method is listening for events related to the window is deactivated. This is also a
	 * required method of the WindowListener class.
	 */
	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	/**
	 * This method is listening for events related to the window is deiconified. This is also a
	 * required method of the WindowListener class.
	 */
	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	/**
	 * This method is listening for events related to the window is iconified. This is also a
	 * required method of the WindowListener class.
	 */
	@Override
	public void windowIconified(WindowEvent e) {

	}

	/**
	 * This method is listening for events related to the window being opened. This is also a
	 * required method of the WindowListener class.
	 */
	@Override
	public void windowOpened(WindowEvent e) {

	}
}
