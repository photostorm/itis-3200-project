/**
 * CryptoUtil: This class holds functions for the encryption and decryption
 * 
 * @author Justin E. Ervin
 * @version 4/15/2015
 */

package net.sslrsa.project;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;

public class CryptoUtil {
	/**
	 * This method converts an byte array to a hex string
	 * 
	 * @param an byte array
	 * @return a string containing a hex string
	 */
	public static String byteArrayToHexString(byte[] array) {
		return DatatypeConverter.printHexBinary(array);
	}

	/**
	 * This method converts a byte array of the key to the object for the private key
	 * 
	 * @param a byte array of the key
	 * @return the object for the private key
	 * @throws InvalidKeySpecException
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 */
	public static PrivateKey convertStringToPrivateKey(byte[] key) throws InvalidKeySpecException, FileNotFoundException, IOException, NoSuchAlgorithmException, NoSuchProviderException {
		KeyFactory factory = KeyFactory.getInstance("RSA", "BC");
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(key);

		return factory.generatePrivate(keySpec);
	}

	/**
	 * This method converts the string to the object for the AES key
	 * 
	 * @param a byte array of the key
	 * @return the object for the AES key
	 */
	public static SecretKey convertStringToSecretKey(byte[] key) {
		SecretKey secretKey = new SecretKeySpec(key, 0, key.length, "AES");

		return secretKey;
	}

	/**
	 * This method decrypts a message with AES key
	 * 
	 * @param the object of the AES key
	 * @param the cipher text of the message
	 * @return the normal text of the message
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static String decryptAES(SecretKey aesKey, String cipherText) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("AES", "BC");

		cipher.init(Cipher.DECRYPT_MODE, aesKey);
		byte[] plainBytes = cipher.doFinal(Base64.decodeBase64(cipherText));

		return new String(plainBytes);
	}

	/**
	 * This method decrypts a message with RSA private key
	 * 
	 * @param the object of the private key
	 * @param the cipher text of the message
	 * @return the normal text of the message
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws NoSuchPaddingException
	 */
	public static String decryptRSA(Key privateKey, String cipherText) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException {
		Cipher cipher = Cipher.getInstance("RSA/None/NoPadding", "BC");

		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		byte[] plainText = cipher.doFinal(Base64.decodeBase64(cipherText));

		return new String(plainText);
	}

	/**
	 * This method encrypts a message with RSA public key
	 * 
	 * @param the object of AES key
	 * @param the message which is going to be encrypted
	 * @return the cipher text of the message
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws NoSuchPaddingException
	 */
	public static String encryptAES(SecretKey aesKey, String input) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException {
		Cipher cipher = Cipher.getInstance("AES", "BC");

		cipher.init(Cipher.ENCRYPT_MODE, aesKey);
		byte[] encryptedBytes = cipher.doFinal(input.getBytes());

		return Base64.encodeBase64String(encryptedBytes);
	}

	/**
	 * This method encrypts a message with RSA public key
	 * 
	 * @param the object of RSA public key
	 * @param the message which is going to be encrypted
	 * @return the cipher text of the message
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws NoSuchPaddingException
	 */
	public static String encryptRSA(Key publicKey, String input) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException {
		Cipher cipher = Cipher.getInstance("RSA/None/NoPadding", "BC");

		cipher.init(Cipher.ENCRYPT_MODE, publicKey, new SecureRandom());
		byte[] cipherText = cipher.doFinal(input.getBytes());

		return Base64.encodeBase64String(cipherText);
	}

	/**
	 * This method generates a AES key
	 * 
	 * @param the size of the key
	 * @return the object of AES key
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 */
	public static SecretKey generateAESKey(int keySize) throws NoSuchAlgorithmException, NoSuchProviderException {
		KeyGenerator KeyGen = KeyGenerator.getInstance("AES");
		SecureRandom random = new SecureRandom();
		KeyGen.init(keySize, random);
		SecretKey aesKey = KeyGen.generateKey();
		return aesKey;
	}

	/**
	 * This method generates a RSA key pair
	 * 
	 * @param the size of the key
	 * @return the object of the root public and private key pair
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 */
	public static KeyPair generateRSAKeyPair(int keySize) throws NoSuchAlgorithmException, NoSuchProviderException {
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");
		SecureRandom random = new SecureRandom();

		generator.initialize(keySize, random);
		KeyPair pair = generator.generateKeyPair();

		return pair;
	}
}
